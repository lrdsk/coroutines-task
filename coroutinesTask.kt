import kotlinx.coroutines.*
import kotlin.concurrent.thread
import kotlin.system.measureTimeMillis


fun main(): Unit = runBlocking {
    var iter: Int = 0
    thread {
        Thread.sleep(1000)
        print("World\n")
        println("I'm sleeping $iter")
    }
    print("Hello ")
    runBlocking {
        delay(2000)
        iter += 1
        println("I'm sleeping $iter")
    }
    val timeSequential = measureTimeMillis {
        val num1 = getNumber1()
        val num2 = getNumber2()
        iter += 1
        println("I'm sleeping $iter")
        iter += 1
        println("I'm sleeping $iter")
        print("sum = ${num1 + num2}, sequential call time = ")
    }
    println(timeSequential)


    val timeAsynchronous = measureTimeMillis {
        launch {
            val num1 = getNumber1()
            val num2 = getNumber2()
            iter += 1
            println("I'm sleeping $iter")
            iter += 1
            println("I'm sleeping $iter")
            print("sum = ${num1 + num2},")

        }
    }

    launch {
        delay(2100)

        println(" asynchronous call time = $timeAsynchronous")
        if (timeSequential > timeAsynchronous) {
            println("timeSequential > timeAsynchronous")
        } else {
            println("timeSequential < timeAsynchronous")
        }
        iter += 1
        println("I'm sleeping $iter")
        println("I'm tired of waiting! I'm running finally main: Now I can quit.")
    }
}

private suspend fun getNumber1(): Int {
    delay(1000)
    return (0..15).random()
}

private suspend fun getNumber2(): Int {
    delay(1000)
    return (0..15).random()
}



